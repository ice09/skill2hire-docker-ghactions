# Continuous Integration with GitLab CI

Our goal is to implement this user story: 

> As a developer, I want my application to be built **if I push new code** and all tests pass. The application should be deployable after the built, **so I can test my changes immediately**.

To test our changes, we have to set up the infrastructure with `docker compose up` and, if we want to use the reverse proxy, the NGINX Docker container as well.

## 👉 Target Architecture

As a result of this quest, this **build pipeline** should be implemented.

![](https://i.imgur.com/mJfE8a7l.png)

We have these components already up and running:

* Maven Build Process
* Spring Boot Maven Docker Plugin

This enables us to create a new Docker image with `mvn spring-boot:build-image`.

We also have to pulling step implemented with Docker Compose:

* Pull Docker image for Docker Compose

If you call `docker-compose up`, the images `ice0nine/docker-demo:staging` and `ice0nine/docker-demo:prod` will be pulled at DockerHub and be started in a local Docker Compose environment (cmp. last quest).

So what is missing? We need a trigger which, once we push new code to build the application on a build server, runs all tests and creates a runnable artifact (the Spring Boot web application in our case).

## 📖 Setup GitLab CI

As we use GitLab as our code repository, it makes sense to let GitLab provide the **Continuous Integration** we want to implement. GitLab does this with **GitLab CI**.

### Configuring GitLab CI for Java & Maven

We will use this GitLab CI config `.gitlab-ci.yml` for our build:

```yaml
image: docker
services:
  - docker:dind

cache:
  paths:
    - .m2/repository
    
variables:
  DOCKER_DRIVER: overlay
  SPRING_PROFILES_ACTIVE: gitlab-ci
  USER_GITLAB: ice09
  APP_NAME: docker-demo
  REPO: skill2hire-docker-ghactions
  DOCKER_HOST: tcp://docker:2375
  MAVEN_OPTS: "-Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository"  

stages:
  - build
  - test
  - docker

maven-compile:
  image: maven:3.8.7-eclipse-temurin-19
  stage: build
  script: "mvn --batch-mode --update-snapshots compile -Dimage.tag=${CI_COMMIT_REF_NAME}"

maven-test:
  image: maven:3.8.7-eclipse-temurin-19
  stage: test
  script: "mvn --batch-mode --update-snapshots test -Dimage.tag=${CI_COMMIT_REF_NAME}"

docker-build-push:
  image: maven:3.8.7-eclipse-temurin-19
  stage: docker
  script: 
    - mvn --batch-mode --update-snapshots spring-boot:build-image -Dimage.tag=${CI_COMMIT_REF_NAME}
    - apt-get update
    - apt install -yq docker.io
    - docker login -u $DOCKER_USERNAME -p $DOCKER_TOKEN
    - docker push ice0nine/docker-demo:${CI_COMMIT_REF_NAME}
```

```resource
https://docs.gitlab.com/ee/ci/quick_start/
# GitLab CI Quick Start
Learn more about GitLab CI.
```

Our `.gitlab-ci.yml` file must be in the repository root folder for GitLab to detect it and start the build pipeline.

On github.com in your repository, you can choose the tab **Actions** if the file exists and is accessible to GitHub. 

![](https://i.imgur.com/2jjVYzZl.png)

![](https://i.imgur.com/NodEMk8l.png)


With the instruction `on: [push]` GitHub is instructed to run the actions on each `git push` to any branch. **Our goal is that the Docker image is generated after each push and successful unit testing and pushed to DockerHub.**

### 🔒 Configure Secrets

> We don't want out credentials for Docker to appear anywhere in our code base. Therefore, we make sure to set the credentials in a vault provided by GitHub for these use cases.

To be able to reference the secrets in the workflow definition like this

```bash
docker login -u $DOCKER_USERNAME -p $DOCKER_TOKEN
```

we have to store them with the correct keys in the Actions secrets and variables

* First, **protect** the `staging` branch.

![](https://i.imgur.com/s247dxHl.png)

* Second, add the variables correctly.

![](https://i.imgur.com/NrxTFXWl.png)

### GitLab CI Definition

After a push you can watch the process on the GitHub Actions tab for this build.

![](https://i.imgur.com/qOKdZW4l.png)

You can see each step with its **name** and the execution result.

In our `on-push.yml`, these steps are defined:

* actions/checkout@v3 (not named)
* Set up JDK 19
* Build with Maven
* Login to DockerHub
* Push

You can drill down into logs of each step during the build and watch the execution:

![](https://i.imgur.com/y46bkaVl.png)

After a successful build, the green checkmark should be assigned to the build. 
If there are errors, you can investigate the logs to find our the reasons.

![](https://i.imgur.com/4cRwdG7l.png)

After successful build, you can log into docker.hub.com to see if the images have been pushed correctly. 
If they have, these new images are now accessible for all users, eg. our `docker-compose.yml`.

![](https://i.imgur.com/yjExxXfl.png)